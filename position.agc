

function ReadPositionFromFile( file as integer )
  pos as Point
  
  pos.x = ReadFloat( file )
  pos.y = ReadFloat( file )
endfunction pos

function WritePositionToFile( pos ref as Point, file as integer )
  WriteFloat( file, pos.x )
  WriteFloat( file, pos.y ) 
endfunction
