
global backgroundFileNames as string[]

function EnumerateBackgroundFiles()
  fileName as String
  
  SetFolder("/media/art/bg/ext")
  fileName = GetFirstFile()
  backgroundFileNames.insert( fileName )
  while fileName <> ""
    fileName = GetNextFile()
    backgroundFileNames.insert( fileName )
  endwhile 
  
  SetFolder("/media")
endfunction

function LoadBackground(fileName as string)
  DeleteSprite(bgSprite)
  bgSprite = LoadSprite(BACKGROUND_PATH + "/ext/" + fileName )
  SetSpriteDepth(bgSprite, BACKGROUND_DEPTH)
  SetSpriteScale(bgSprite, BACKGROUND_SCALE_X, BACKGROUND_SCALE_Y)
  if(fullScreenMode = YES)
    MoveBackgroundFullScreen()
  else
    MoveBackgroundUp()
  endif
endfunction

function MoveBackgroundUp()
  SetSpritePosition(bgSprite, 0, -VERTICAL_SHIFT)
endfunction

function MoveBackgroundFullScreen()
  SetSpritePosition(bgSprite, 0, 0)
endfunction

function LoadCurrentBackground()
  LoadBackground( backgroundFileNames[currentBackgroundId] )
endfunction
