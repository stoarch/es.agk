#constant YES 1
#constant NO 0

#constant NOT_LOADED 2
#constant NO_VIDEO 0
#constant VIDEO_LOADED 1
#constant VIDEO_NOT_SUPPORTED -1


#constant MAX_EMOTIONS 20
#constant MAX_BODIES 10
#constant MAX_OUTFITS 10

#constant EMOTION_TENDER 1
#constant EMOTION_SCARED 2
#constant EMOTION_SUPRISE 3
#constant EMOTION_SAD 4
#constant EMOTION_ANGRY 5
#constant EMOTION_SMILE2 6
#constant EMOTION_SHY 7
#constant EMOTION_LAUGH 8
#constant EMOTION_HAPPY 9
#constant EMOTION_SMILE 10
#constant EMOTION_SERIOUS 11
#constant EMOTION_NORMAL 12
#constant EMOTION_RAGE 13
#constant EMOTION_SHOCKED 14
#constant EMOTION_GRIN 15

#constant MIN_EMOTION 1
#constant MAX_EMOTION 12

#constant BODY_1 1
#constant BODY_2 2
#constant BODY_3 3
#constant BODY_4 4

#constant OUTFIT_DRESS 1
#constant OUTFIT_PIONEER 2
#constant OUTFIT_SPORT 3
#constant OUTFIT_SWIM 4
#constant OUTFIT_PIONEER2 5

#constant MIN_OUTFIT 1
#constant MAX_OUTFIT 4

#constant CHAR_OFFSET_X 150
#constant CHAR_OFFSET_Y 100

#constant SCREEN_HEIGHT 720
#constant SCREEN_WIDTH 1280

#constant SCENE_CENTER_X 1280/2
#constant SCENE_CENTER_Y 720/2

#constant VERTICAL_SHIFT 150

#constant BODY_DEPTH 3
#constant EMOTIONS_DEPTH 1
#constant OUTFIT_DEPTH 2
#constant BACKGROUND_DEPTH 120

#constant BACKGROUND_PATH "art/bg"
#constant SPRITE_PATH "art/sprites/full/"


#constant BODY_PATH "body/"
#constant EMOTIONS_PATH "emotions/"
#constant OUTFIT_PATH "outfit/"

#constant CHAR_SPRITE_SCALE_X = 0.2
#constant CHAR_SPRITE_SCALE_Y = 0.2

#constant BACKGROUND_SCALE_X = 0.68
#constant BACKGROUND_SCALE_Y = 0.68

#constant CMD_CHAR_SHOW 1
#constant CMD_CHAR_HIDE 2
#constant CMD_CHAR_MOVE 3
#constant CMD_CHAR_CHANGE_EMOTION 4
#constant CMD_CHAR_CHANGE_OUTFIT 5

#constant DRESS_SHIFT_X 1

#constant VISIBLE 1
#constant INVISIBLE 0

#constant HIGHLIGHT_BLUE 128
#constant NORMAL_BLUE 255

#constant KEY_LEFT 37
#constant KEY_RIGHT 39
#constant KEY_ESCAPE 27
#constant KEY_SPACE 32
#constant KEY_UP 38
#constant KEY_DOWN 40
#constant KEY_PLUS 107
#constant KEY_MINUS 109
#constant KEY_COMMA 188
#constant KEY_POINT 190
#constant KEY_PGUP 33
#constant KEY_PGDOWN 34

#constant KEY_0            48 `(triggered by both top row number keys and numpad keys)
#constant KEY_1            49
#constant KEY_2            50
#constant KEY_3            51
#constant KEY_4            52
#constant KEY_5            53
#constant KEY_6            54
#constant KEY_7            55
#constant KEY_8            56
#constant KEY_9            57

#constant KEY_F1           112
#constant KEY_F2           113
#constant KEY_F3           114
#constant KEY_F4           115
#constant KEY_F5           116
#constant KEY_F6           117
#constant KEY_F7           118
#constant KEY_F8           119
#constant KEY_F9           120
#constant KEY_F10          121
#constant KEY_F11          122
#constant KEY_F12          123


#constant KEY_A            65
#constant KEY_B            66
#constant KEY_C            67
#constant KEY_D            68
#constant KEY_E            69
#constant KEY_F            70
#constant KEY_G            71
#constant KEY_H            72
#constant KEY_I            73
#constant KEY_J            74
#constant KEY_K            75
#constant KEY_L            76
#constant KEY_M            77
#constant KEY_N            78
#constant KEY_O            79
#constant KEY_P            80
#constant KEY_Q            81
#constant KEY_R            82
#constant KEY_S            83
#constant KEY_T            84
#constant KEY_U            85
#constant KEY_V            86
#constant KEY_W            87
#constant KEY_X            88
#constant KEY_Y            89
#constant KEY_Z            90

#constant    KEY_BACK         8
#constant    KEY_TAB          9
#constant    KEY_ENTER        13
#constant    KEY_SHIFT        16
#constant    KEY_CONTROL      17
#constant    KEY_ALT          18
#constant    KEY_PAUSE        19
#constant    KEY_CAPSLOCK     20


#constant KEY_STATE_DOWN 1
#constant KEY_STATE_UP 0

#constant DIALOGUE_BOX_PATH = "art/ui/dialogue_box/day/"

#constant MAX_TEXT_LINES 3
#constant TEXT_LINES_SIZE 25
#constant TEXT_LINES_SHIFT 5

#constant FILLED_BOX 1
#constant EMPTY_BOX 0

#constant MOUSE_BUTTON_PRESSED 1
#constant MOUSE_BUTTON_RELEASED 0

#constant SAVE_FILE_NAME "data1.sav"
#constant LOG_FILE_NAME "es.agk.log"

#constant FILE_APPEND 1
#constant FILE_OVERWRITE 0

#constant SAVE_FILE_VERSION 3
