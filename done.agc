` HISTORY OF DONE:
`
`(28.06.2019)
` +I Character now has name
` +I(2) Alisa and slavya now has id and name as well
` +F Debug information for character shown
` +I Background extracted
` +B Alisa stores emotion and outfit on preparation
` +F Saving photo introduced
` +F(2) Hiding Slavya and Alisa introduced
` +I Character now has visible fields
` +I(2) Showing/hiding character manages visible field
` +I(2) Highlight functions now uses visibility (for Slavya and Alisa)
` +B Hidden debug info with character
`
` 27.06.2019
` +B Fixed selected char remains when selecting another
` +F(2) Save/Load characters from file
` +I(3) Saving Slavya, Alisa and Point to file
` +I(3) Loading Slavya, Alisa and Point from file
` +F Logging added
`
`26.06.2019
` +I Character now has own body outfits
` +I(2) Character showing now uses file inside structure
` +I Character logic extracted to file
` +I Constants extracted to file
` +B Alisa dir of images fixed 
` +I Character now has own depth layer
` +I(3) Movement and scaling now commonized and check for active char
` +I Active character storing added
` +F Selection of character added (1,2 keys)
`
` 25.06.2019
` +B Movement by keyboard and mouse split
` +F(2) Zoom in/out with mouse wheel
` +I Now dialog hidden by default
` +F Move Slavya with mouse added
` +F Draw box around Slavya when selected
` +F(2) Highlight Slavya with mouse
` +I(3) Renamed character methods to be more OOP-like (prefix to start)

` 22.06.2019 
` +F(2) Change Slayva emotions with keyboard (left,right)
` +F(2) Change Slavya outfit with keyboard (up,down)
` +F(2) Change Slavya zoom factor by 0.1 fluently
` +I Character now uses currentZoom
` +I Character movement and outfit changing now uses currentPosition
` +F(4) Movement character with wasd
` +B Loading background images break loading sprites (setFolder!)
` +F(3) Background changing with pg up/down and loading filenames
` +F(2) Fullscreen on/off (removing dialogue)
` +B Fixed Slavya fit to fullscreen and out of it
` +B Changing background now look for fullscreen mode status

