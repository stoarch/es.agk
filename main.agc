
// Project: ES.AGK
// Goal: Make port of Everlasting Summer from RenPy to AGK (for export to Android/HTML5)
// Created: 2016-05-21
//
// Author: Vladimir Afonin (mailto: stormarchitextor@gmail.com) (c) 2018-2019
#include "constants.agc"
#include "character.agc"
#include "slavya.agc"
#include "alisa.agc"
#include "position.agc"
#include "background.agc"



SetWindowTitle( "Everlasting Summer AGK" )
SetWindowSize( GetMaxDeviceWidth(), GetMaxDeviceHeight(), 0 )
SetOrientationAllowed( NO, NO, YES, YES ) `only landscape orientation allowed on mobile

// set display properties
SetVirtualResolution( SCREEN_WIDTH, SCREEN_HEIGHT )

global video_status = NOT_LOADED

function PlayIntroVideo()
  video_status = LoadVideo("opening.avi")
  SetVideoDimensions(0,0,SCREEN_WIDTH,SCREEN_HEIGHT)
  PlayVideo()
endfunction



SetGenerateMipmaps(YES)

Type Point
  x as float
  y as float
EndType


Type StoryCommand
  code as integer
  characterCode as integer

  moveKind as integer
  tweenType as integer
  startPos as Point
  endPos as Point

  emotionKind as integer
  outfitKind as integer
EndType

//Line of story with command to setup environment to it
Type StoryLine
  setupCommand as StoryCommand[]
  lineText as string
EndType

Type Story
  lines as StoryLine[]
  background as integer
  ambientSound as integer
  music as integer
EndType


InitSlavya()
InitAlisa()

function InitBodyOutfits()
  InitSlavyaBodyOutfits()
  InitAlisaBodyOutfits()
endfunction



global fullScreenMode as integer
fullScreenMode = NO

global dialogueBox as integer
dialogueBox = LoadSprite(DIALOGUE_BOX_PATH + "dialogue_box.png")
SetSpriteScale( dialogueBox, 0.8, 0.8 )
SetSpritePosition( dialogueBox, 10, SCREEN_HEIGHT - GetSpriteHeight(dialogueBox) - 10)



``` MAIN ```

`### MAIN INIT ###``
InitBodyOutfits()


`PlayIntroVideo()


sprite_index = 0

global bgSprite as integer
global currentBackgroundId as integer

EnumerateBackgroundFiles()
currentBackgroundId = backgroundFileNames.length - 1

LoadCurrentBackground()

UseNewDefaultFonts(1)

global textLines as integer[MAX_TEXT_LINES]
for i = 1 to MAX_TEXT_LINES
  textLines[i] = CreateText( "" )
  SetTextSize(textLines[i], TEXT_LINES_SIZE)
  SetTextPosition( textLines[i], 50, SCREEN_HEIGHT - 125 + i * TEXT_LINES_SIZE + TEXT_LINES_SHIFT )
next i

function ShowText( msg as string )
  SetTextVisible(textLines[1], VISIBLE)
  
  if(msg <> "@@@")then SetTextString(textLines[1],  msg )
endfunction

function HideText()
  SetTextVisible(textLines[1], INVISIBLE)
endfunction

ShowText( "Оттуда выглянула девочка...")

global nudeMode as integer
nudeMode = NO 

global currentEmotion as integer
global currentOutfit as integer

currentEmotion = EMOTION_SMILE
currentOutfit = OUTFIT_PIONEER


global currentZoom as float
currentZoom = 0.2

#constant DELTA_ZOOM 0.005
#constant DELTA_SHIFT 5

global currentPos as Point
currentPos.x = SCREEN_WIDTH/2
currentPos.y = SCREEN_HEIGHT/3

ShowSlavya(currentEmotion, currentOutfit)
ScaleSlavya( currentZoom )
MoveSlavya( currentPos.x, currentPos.y )

ShowAlisa(currentEmotion, currentOutfit)
ScaleAlisa( currentZoom )
MoveAlisa( currentPos.x - 550, currentPos.y )

mouseX as float
mouseY as float
characterSelected as integer


blueColor as integer
blueColor = MakeColor(0,0,255)

HideText()
MoveBackgroundFullScreen()
fullScreenMode = YES
visibleStatus = INVISIBLE
SetSpriteVisible(dialogueBox, visibleStatus)

ShowSlavyaAgain()
ShowAlisaAgain()

#constant SLAVYA_CHAR_ID 1
#constant ALISA_CHAR_ID 2

global activeCharId as integer
activeCharId = SLAVYA_CHAR_ID

`` LOG ``
global logText as integer
logText = CreateText("LOG:")
SetTextPosition( logText, 10, 10 )
SetTextSize( logText, 15 )

`` FUNCTIONS ``

function ShowActiveCharacterAgain()
  select activeCharId
    case SLAVYA_CHAR_ID: 
      ShowSlavyaAgain()
    endcase
    case ALISA_CHAR_ID: 
      ShowAlisaAgain()
    endcase
  endselect
endfunction


function LogMessage( msg as string )
  logFile as integer
  logFile = OpenToWrite(LOG_FILE_NAME,FILE_APPEND)
  
  WriteLine(logFile, GetCurrentDate() + " " + GetCurrentTime() + " >> " + msg)
  
  SetTextString( logText, msg )
  
  CloseFile(logFile)
endfunction

function LoadFromSaveFile()
  if(not GetFileExists(SAVE_FILE_NAME))then exitfunction
    
  LogMessage("Loading from save file...")
    
  saveFile as integer
  saveFile = OpenToRead(SAVE_FILE_NAME)
  
  version as integer
  version = ReadInteger(saveFile)
  
  if( version < SAVE_FILE_VERSION ) ` we can partially read newer files (structure only expanded) but we can not figure out how much to read from old one
    LogMessage("Error: Save file is other version than we can read:" + str(version) + " instead of " + str(SAVE_FILE_VERSION))
    exitfunction
  endif
  
  ReadSlavyaFromFile( saveFile )
  ReadAlisaFromFile( saveFile )  
  
  CloseFile( saveFile )
  
  LogMessage("Completed loading from save.")
endfunction

function WriteToSaveFile()
  LogMessage("Writing to save")
  
  saveFile as integer
  saveFile = OpenToWrite(SAVE_FILE_NAME)
  
  WriteInteger( saveFile, SAVE_FILE_VERSION )
  
  WriteSlavyaToFile( saveFile )
  WriteAlisaToFile( saveFile )
  
  CloseFile( saveFile )
  
  LogMessage("Completed writing to save")
endfunction

LoadFromSaveFile()

`` ACTIVE CHARACTER ``
` Copy our active char data to their respective char
function UpdateActiveCharacter()
  select activeCharId
    case SLAVYA_CHAR_ID:
      slavyaChar = activeCharacter
    endcase
    case ALISA_CHAR_ID:
      alisaChar = activeCharacter
    endcase
  endselect
endfunction

`` COLORS ``
global colorBlue as integer
colorBlue = MakeColor(0,0,255)

`DEBUG texts
global debugInfoShown as integer
global debugTexts as integer[5]

#constant DEBUG_TEXT_SHIFT 100
#constant DEBUG_TEXT_ROW_HEIGHT 30
#constant DEBUG_TEXT_SIZE 15
#constant DEBUG_TEXT_WIDTH 250

function InitDebugInfo()
  debugInfoShown = YES
  
  debugTexts[0] = CreateText("Character")
  debugTexts[1] = CreateText("Position")
  debugTexts[2] = CreateText("Scale")
  debugTexts[3] = CreateText("Depth")
  debugTexts[4] = CreateText("Emotion and outfit")

  for i = 0 to debugTexts.length 
    SetTextSize( debugTexts[i], DEBUG_TEXT_SIZE )
  next i
endfunction

function ShowDebugInfoFor( ch ref as Character )
  x as float
  y as float
  
  if( debugInfoShown = NO )then exitfunction
  
  x = SCREEN_WIDTH - DEBUG_TEXT_WIDTH - 10
  y = 10
  
  for i = 0 to debugTexts.length 
    SetTextVisible( debugTexts[i], VISIBLE )
  next i

  for i = 0 to debugTexts.length 
    SetTextPosition( debugTexts[i], x, y + i*DEBUG_TEXT_ROW_HEIGHT)
  next i
  
  SetTextString( debugTexts[0], "Character: " + ch.name )
  SetTextString( debugTexts[1], "Position: x:" + str(round(ch.pos.x)) + " y:" + str(round(ch.pos.y)))
  SetTextString( debugTexts[2], "Scale: " + str(ch.scale))
  SetTextString( debugTexts[3], "Depth: " + str(ch.depth))
  SetTextString( debugTexts[4], "Emotion: " + str(ch.emotion) + " outfit:" + str(ch.outfit))
  
  DrawBox( x, y, x + DEBUG_TEXT_WIDTH, y + debugTexts.length*DEBUG_TEXT_ROW_HEIGHT, colorBlue, colorBlue, colorBlue, colorBlue, EMPTY_BOX )
endfunction

function HideDebugInfo()
  for i = 0 to debugTexts.length 
    SetTextVisible( debugTexts[i], INVISIBLE )
  next i
endfunction

InitDebugInfo()

LogMessage("App started")

`` MAIN ``
do 
  dt# = GetFrameTime()

  UpdateAllTweens(dt#)

  mouseX = GetRawMouseX()
  mouseY = GetRawMouseY()

  pointX as float
  pointX = ScreenToWorldX(mouseX)
  
  pointY as float
  pointY = ScreenToWorldY(mouseY)

  //Update slavya scale
  if(GetTweenCustomPlaying(slavyaScaleTween) = YES)
    scale# = GetTweenCustomFloat1(slavyaScaleTween)
    ScaleSlavya(scale#)
  endif

  `` EMOTIONS AND OUTFIT ``
  if(GetRawKeyReleased(KEY_LEFT))
    currentEmotion = GetActiveCharacterEmotion()
    dec currentEmotion 
    if currentEmotion < MIN_EMOTION then currentEmotion = MAX_EMOTION
    SetActiveCharacterEmotion(currentEmotion)
    
    ShowActiveCharacterAgain()
  endif
  
  if(GetRawKeyReleased(KEY_RIGHT))
    currentEmotion = GetActiveCharacterEmotion()
    inc currentEmotion
    if( currentEmotion > MAX_EMOTION )then currentEmotion = MIN_EMOTION
    SetActiveCharacterEmotion(currentEmotion)

    ShowActiveCharacterAgain()
  endif
  
  if(GetRawKeyReleased(KEY_UP))
    currentOutfit = GetActiveCharacterOutfit()
    dec currentOutfit
    if(currentOutfit < MIN_OUTFIT)then currentOutfit = MAX_OUTFIT
    SetActiveCharacterOutfit(currentOutfit)
    
    ShowActiveCharacterAgain()
  endif
  
  if(GetRawKeyReleased(KEY_DOWN))
    currentOutfit = GetActiveCharacterOutfit()
    inc currentOutfit
    if(currentOutfit > MAX_OUTFIT)then currentOutfit = MIN_OUTFIT
    SetActiveCharacterOutfit(currentOutfit)

    ShowActiveCharacterAgain()
  endif
  
  `` NUDE MODE ``
	if(GetRawKeyReleased(KEY_N))
		nudeMode = 1 - nudeMode
    ShowActiveCharacterAgain()
	endif
  
  `` ZOOM ``
  if(GetRawKeyState(KEY_POINT) = KEY_STATE_DOWN )
    currentZoom = GetActiveCharacterScale()
    inc currentZoom, DELTA_ZOOM
    ScaleActiveCharacter( currentZoom )
  endif

  if(GetRawKeyState(KEY_COMMA) = KEY_STATE_DOWN)
    currentZoom = GetActiveCharacterScale()
    currentZoom = currentZoom - DELTA_ZOOM
    ScaleActiveCharacter( currentZoom )
  endif
  
  if(GetRawMouseWheelDelta() < 0)
    currentZoom = GetActiveCharacterScale()
    inc currentZoom, DELTA_ZOOM
    ScaleActiveCharacter( currentZoom )
  endif

  if(GetRawMouseWheelDelta() > 0)
    currentZoom = GetActiveCharacterScale()
    dec currentZoom, DELTA_ZOOM
    ScaleActiveCharacter( currentZoom )
  endif

  
  `` MOVEMENT `` 
  
  `` MOUSE MOVEMENT ``
  mouseSprite = GetSpriteHit( pointX, pointY )
  characterSelected = NO
  
  spriteX as float
  spriteY as float
  
  spriteX = GetSpriteXByOffset(activeCharacter.bodySprite)
  spriteY = GetSpriteYByOffset(activeCharacter.bodySprite)
  
   
  dx as float
  dy as float
  if(GetRawMouseLeftPressed())
    dx = pointX - spriteX
    dy = pointY - spriteY
  endif

  characterSelected = NO
  if(mouseSprite = slavyaChar.bodySprite)or(mouseSprite = slavyaChar.emotionSprite)or(mouseSprite = slavyaChar.outfitSprite)
    characterSelected = YES
    SelectSlavyaAsActiveCharacter()
    DeselectAlisa()
  elseif(mouseSprite = alisaChar.bodySprite)or(mouseSprite = alisaChar.emotionSprite)or(mouseSprite = alisaChar.outfitSprite)
    characterSelected = YES
    SelectAlisaAsActiveCharacter()
    DeselectSlavya()
  endif

  select activeCharId
    case SLAVYA_CHAR_ID:
      if(slavyaChar.isVisible = VISIBLE )               
        if( IsSlavyaSelected() and (characterSelected = YES))
          ShowSlavyaHighlighted()
        else
          ShowSlavyaNormal()
          HideDebugInfo()
        endif     
      endif
    endcase
    case ALISA_CHAR_ID:
      if(alisaChar.isVisible = VISIBLE)        
        if( IsAlisaSelected() and (characterSelected = YES))
          ShowAlisaHighlighted()
        else
          ShowAlisaNormal()
          HideDebugInfo()
        endif     
      endif
    endcase
  endselect
  
  if(GetRawMouseLeftState() = MOUSE_BUTTON_PRESSED)
    currentPos = GetActiveCharacterPos()
    currentPos.x = pointX - dx
    currentPos.y = pointY - dy
    MoveActiveCharacterToCurrentPos(currentPos.x, currentPos.y)
  endif
  
  `` KEYBOARD MOVEMENT ``
  if(GetRawKeyState(KEY_W) = KEY_STATE_DOWN)
    currentPos = GetActiveCharacterPos()
    dec currentPos.y, DELTA_SHIFT
    MoveActiveCharacterToCurrentPos(currentPos.x, currentPos.y)
  endif

  if(GetRawKeyState(KEY_S) = KEY_STATE_DOWN)
    currentPos = GetActiveCharacterPos()
    inc currentPos.y, DELTA_SHIFT
    MoveActiveCharacterToCurrentPos(currentPos.x, currentPos.y)
  endif
  
  if(GetRawKeyState(KEY_A) = KEY_STATE_DOWN)
    currentPos = GetActiveCharacterPos()
    dec currentPos.x, DELTA_SHIFT
    MoveActiveCharacterToCurrentPos(currentPos.x, currentPos.y)
  endif

  if(GetRawKeyState(KEY_D) = KEY_STATE_DOWN)
    currentPos = GetActiveCharacterPos()
    inc currentPos.x, DELTA_SHIFT
    MoveActiveCharacterToCurrentPos(currentPos.x, currentPos.y)
  endif
    
  ShowDebugInfoFor(activeCharacter)
  
  `` CHARACTER SELECTION ``
  if(GetRawKeyReleased(KEY_1))
    ShowSlavyaNormal()
    SelectSlavyaAsActiveCharacter()
    
    if(GetRawKeyState(KEY_CONTROL) = KEY_STATE_DOWN )
      HideSlavya()
      HideDebugInfo()
    else
      ShowSlavyaAgain()
      ShowSlavyaNormal()
    endif
  endif
  
  
  if(GetRawKeyReleased(KEY_2))
    ShowSlavyaNormal()
    SelectAlisaAsActiveCharacter()
    
    if(GetRawKeyState(KEY_CONTROL) = KEY_STATE_DOWN )
      HideAlisa()
      HideDebugInfo()
    else
      ShowAlisaAgain()
      ShowAlisaNormal()
    endif
  endif

  `` BACKGROUND ``
  if(GetRawKeyReleased(KEY_PGUP))
    dec currentBackgroundId
    if( currentBackgroundId < 0 )then currentBackgroundId = backgroundFileNames.length - 1
    LoadCurrentBackground()
  endif

  if(GetRawKeyReleased(KEY_PGDOWN))
    inc currentBackgroundId
    if( currentBackgroundId > backgroundFileNames.length - 1)then currentBackgroundId = 0
    LoadCurrentBackground()
  endif
  
  `` SAVE SYSTEM ``
  if(GetRawKeyPressed(KEY_F6))
    LoadFromSaveFile()
  endif
  
  if(GetRawKeyPressed(KEY_F5))
    WriteToSaveFile()
  endif
  
  `` DIALOGUE ``
  if(GetRawKeyReleased(KEY_H))
    visibleStatus = 1 - GetSpriteVisible(dialogueBox)
    SetSpriteVisible(dialogueBox, visibleStatus)
    
    if(visibleStatus = INVISIBLE)
      HideText()
      MoveBackgroundFullScreen()
      fullScreenMode = YES
    else
      ShowText("@@@") `same text again
      MoveBackgroundUp()
      fullScreenMode = NO
    endif

    ShowSlavyaAgain()
  endif
  
  if( video_status = NO_VIDEO )
    Print("Video can not be loaded")
  endif

  if(GetPointerPressed() = 1)
    StopVideo()
  endif

  if(GetRawKeyPressed(KEY_SPACE) = YES)
    ShowSlavyaAtCenterZoomed(EMOTION_NORMAL, OUTFIT_PIONEER)
    ShowText("... в пионерской форме")
    ZoomInSlavyaAnimated()
  endif

  if(GetRawKeyPressed(KEY_ESCAPE) = YES)
    exit
  endif
  
  `` SAVE SCREENSHOT ``
  if(GetRawKeyPressed(KEY_F2))
    Render()
    
    screenImage as integer
    screenImage = GetImage(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)
    
    SaveImage(screenImage, "photo" + str(GetUnixTime()) + ".png")
    DeleteImage(screenImage)
    
    ClearScreen()
  endif


  Sync()
loop


` Version (0.30.64.7) (epic, feature, improvement, bug fix)

`^^^`
` E ..... .....
` F ..... .....
` I ..... .....
` B ..... .....

`DONE:
`(30.06.2019)
` +I(2) Active character emotion/outfit set by method
` +I(2) Active character emotion/outfit selected from it
` +I Active character highlighted only on mouse on it
` +I Active character copied to Slavya/Alisa when updated
` +I(2) Active character updated data for pos/scale every time
` +I Sprite X/Y get from active character
` +I Debug info shown in top right border
` +I(2) Scale now uses active character scale and stores to it
` +I(4) WASD now uses active character pos and stores to it
` +I(2) Get active character scale/pos
` +B(2) Movement of active character with x,y params
` +I(2) Keyboard selection of active character delegated to their method
` +I Active character global var introduced
` +I(2) Active character management (move/scale) extracted
` +I(2) Active character now can be selected (Alisa/Slavya)
` +B(2) Alisa/Slavya does not deselected when mouse over other active char
` +I(2) Alisa and Slavya now has highlight code
` +I(4) Alisa and Slavya can be selected/deselected
` +F Selection of active character by mouse
` +I(2) Alisa and Slavya now handles normal highlight by themselves
`
`DOING:
` ?
`DO NEXT:
` - Debugging loading of character
`
`TODO:
` - Introduce language selection
` - Store language specific strings in text file and load it


