#constant SLAVYA_BODY_SPRITE 10000
#constant SLAVYA_EMOTION_SPRITE 10001
#constant SLAVYA_OUTFIT_SPRITE 10002

#constant SLAVYA_BODY_1_IMG "sl_1_body.png"
#constant SLAVYA_BODY_2_IMG "sl_2_body.png"
#constant SLAVYA_BODY_3_IMG "sl_3_body.png"
#constant SLAVYA_BODY_4_IMG "sl_4_body.png"

#constant SLAVYA_EMOTION_NORMAL_B1_IMG "sl_1_normal.png"
#constant SLAVYA_EMOTION_SERIOUS_B1_IMG "sl_1_serious.png"
#constant SLAVYA_EMOTION_SMILE_B1_IMG "sl_1_smile.png"
#constant SLAVYA_EMOTION_HAPPY_B2_IMG "sl_2_happy.png"
#constant SLAVYA_EMOTION_LAUGH_B2_IMG "sl_2_laugh.png"
#constant SLAVYA_EMOTION_SHY_B2_IMG "sl_2_shy.png"
#constant SLAVYA_EMOTION_SMILE2_B2_IMG "sl_2_smile2.png"
#constant SLAVYA_EMOTION_ANGRY_B3_IMG "sl_3_angry.png"
#constant SLAVYA_EMOTION_SAD_B3_IMG "sl_3_sad.png"
#constant SLAVYA_EMOTION_SURPRISE_B3_IMG "sl_3_surprise.png"
#constant SLAVYA_EMOTION_SCARED_B4_IMG "sl_4_scared.png"
#constant SLAVYA_EMOTION_TENDER_B4_IMG "sl_4_tender.png"

#constant SLAVYA_OUTFIT_B1_PIONEER "sl_1_pioneer.png"
#constant SLAVYA_OUTFIT_B2_PIONEER "sl_2_pioneer.png"
#constant SLAVYA_OUTFIT_B3_PIONEER "sl_3_pioneer.png"
#constant SLAVYA_OUTFIT_B4_PIONEER "sl_4_pioneer.png"

#constant SLAVYA_OUTFIT_B1_DRESS "sl_1_dress.png"
#constant SLAVYA_OUTFIT_B2_DRESS "sl_2_dress.png"
#constant SLAVYA_OUTFIT_B3_DRESS "sl_3_dress.png"
#constant SLAVYA_OUTFIT_B4_DRESS "sl_4_dress.png"

#constant SLAVYA_OUTFIT_B1_SWIM "sl_1_swim.png"
#constant SLAVYA_OUTFIT_B2_SWIM "sl_2_swim.png"
#constant SLAVYA_OUTFIT_B3_SWIM "sl_3_swim.png"
#constant SLAVYA_OUTFIT_B4_SWIM "sl_4_swim.png"

#constant SLAVYA_OUTFIT_B1_SPORT "sl_1_sport.png"
#constant SLAVYA_OUTFIT_B2_SPORT "sl_2_sport.png"
#constant SLAVYA_OUTFIT_B3_SPORT "sl_3_sport.png"
#constant SLAVYA_OUTFIT_B4_SPORT "sl_4_sport.png"


#constant SLAVYA_PATH "sl/"


#constant SLAVYA_OFFSET_X 100
#constant SLAVYA_OFFSET_Y 50

#constant SLAVYA_DEPTH 10
#constant SLAVYA_CHAR_NAME "Славя" `ru 'Slavya' en 

global slavyaChar as Character

function InitSlavya()
  slavyaChar.id = SLAVYA_CHAR_ID
  slavyaChar.name = SLAVYA_CHAR_NAME
  
  slavyaChar.depth = SLAVYA_DEPTH
  
  slavyaChar.bodySprite = SLAVYA_BODY_SPRITE
  slavyaChar.emotionSprite = SLAVYA_EMOTION_SPRITE
  slavyaChar.outfitSprite = SLAVYA_OUTFIT_SPRITE

  LoadSprite(SLAVYA_BODY_SPRITE, SPRITE_PATH + SLAVYA_PATH + BODY_PATH + SLAVYA_BODY_1_IMG)
  SetSpriteDepth(SLAVYA_BODY_SPRITE, BODY_DEPTH)
  SetSpriteScale(SLAVYA_BODY_SPRITE, CHAR_SPRITE_SCALE_X, CHAR_SPRITE_SCALE_Y)
  SetSpriteVisible( SLAVYA_BODY_SPRITE, INVISIBLE )
  SetSpriteOffset( SLAVYA_BODY_SPRITE, SLAVYA_OFFSET_X, SLAVYA_OFFSET_Y )

  LoadSprite(SLAVYA_EMOTION_SPRITE, SPRITE_PATH + SLAVYA_PATH + EMOTIONS_PATH + SLAVYA_EMOTION_SMILE_B1_IMG )
  SetSpriteDepth(SLAVYA_EMOTION_SPRITE, EMOTIONS_DEPTH)
  SetSpriteScale(SLAVYA_EMOTION_SPRITE, CHAR_SPRITE_SCALE_X, CHAR_SPRITE_SCALE_Y )
  SetSpriteVisible( SLAVYA_EMOTION_SPRITE, INVISIBLE )
  SetSpriteOffset( SLAVYA_EMOTION_SPRITE, SLAVYA_OFFSET_X, SLAVYA_OFFSET_Y )

  LoadSprite(SLAVYA_OUTFIT_SPRITE, SPRITE_PATH + SLAVYA_PATH + OUTFIT_PATH + SLAVYA_OUTFIT_B1_PIONEER)
  SetSpriteDepth(SLAVYA_OUTFIT_SPRITE, OUTFIT_DEPTH)
  SetSpriteScale(SLAVYA_OUTFIT_SPRITE, CHAR_SPRITE_SCALE_X, CHAR_SPRITE_SCALE_Y )
  SetSpriteVisible( SLAVYA_OUTFIT_SPRITE, INVISIBLE )
  SetSpriteOffset( SLAVYA_OUTFIT_SPRITE, SLAVYA_OFFSET_X, SLAVYA_OFFSET_Y )
endfunction

function ShowSlavya( emotion, dress )
  PrepareToShowSlavyaWith( emotion, dress )
  CharacterShow( slavyaChar )
endfunction

function PrepareToShowSlavyaWith( emotion, dress )
  body as String
  outfit as String
  emotionFile as String
  
  slavyaChar.emotion = emotion
  slavyaChar.outfit = dress
  
  body = SLAVYA_BODY_1_IMG
  outfit = SLAVYA_OUTFIT_B1_PIONEER
  emotionFile = SLAVYA_EMOTION_NORMAL_B1_IMG

  select emotion
    case EMOTION_ANGRY
      body = SLAVYA_BODY_3_IMG
      outfit = slavyaChar.outfitForBody[3, dress]
      emotionFile = SLAVYA_EMOTION_ANGRY_B3_IMG
    endcase
    case EMOTION_HAPPY
      body = SLAVYA_BODY_2_IMG
      outfit = slavyaChar.outfitForBody[2, dress]
      emotionFile = SLAVYA_EMOTION_HAPPY_B2_IMG
    endcase
    case EMOTION_LAUGH
      body = SLAVYA_BODY_2_IMG
      outfit = slavyaChar.outfitForBody[2, dress]
      emotionFile = SLAVYA_EMOTION_LAUGH_B2_IMG
    endcase
    case EMOTION_NORMAL
      body = SLAVYA_BODY_1_IMG
      outfit = slavyaChar.outfitForBody[1, dress]
      emotionFile = SLAVYA_EMOTION_NORMAL_B1_IMG
    endcase
    case EMOTION_SAD
      body = SLAVYA_BODY_3_IMG
      outfit = slavyaChar.outfitForBody[3, dress]
      emotionFile = SLAVYA_EMOTION_SAD_B3_IMG
    endcase
    case EMOTION_SCARED
      body = SLAVYA_BODY_4_IMG
      outfit = slavyaChar.outfitForBody[4, dress]
      emotionFile = SLAVYA_EMOTION_SCARED_B4_IMG
    endcase
    case EMOTION_SERIOUS
      body = SLAVYA_BODY_1_IMG
      outfit = slavyaChar.outfitForBody[1, dress]
      emotionFile = SLAVYA_EMOTION_SERIOUS_B1_IMG
    endcase
    case EMOTION_SHY
      body = SLAVYA_BODY_2_IMG
      outfit = slavyaChar.outfitForBody[2, dress]
      emotionFile = SLAVYA_EMOTION_SHY_B2_IMG
    endcase
    case EMOTION_SMILE2
      body = SLAVYA_BODY_2_IMG
      outfit = slavyaChar.outfitForBody[2, dress]
      emotionFile = SLAVYA_EMOTION_SMILE2_B2_IMG
    endcase
    case EMOTION_SMILE
      body = SLAVYA_BODY_1_IMG
      outfit = slavyaChar.outfitForBody[1, dress]
      emotionFile = SLAVYA_EMOTION_SMILE_B1_IMG
    endcase
    case EMOTION_SUPRISE
      body = SLAVYA_BODY_3_IMG
      outfit = slavyaChar.outfitForBody[3, dress]
      emotionFile = SLAVYA_EMOTION_SURPRISE_B3_IMG
    endcase
    case EMOTION_TENDER
      body = SLAVYA_BODY_4_IMG
      outfit = slavyaChar.outfitForBody[4, dress]
      emotionFile = SLAVYA_EMOTION_TENDER_B4_IMG
    endcase
  endselect  

  slavyaChar.bodyFile = SPRITE_PATH + SLAVYA_PATH + BODY_PATH + body
  slavyaChar.outfitFile = SPRITE_PATH + SLAVYA_PATH + OUTFIT_PATH + outfit
  slavyaChar.emotionFile = SPRITE_PATH + SLAVYA_PATH + EMOTIONS_PATH + emotionFile
endfunction

`## SLAVYA MANAGEMENT ##
function ShowSlavyaAtCenter(emotion, outfit)
  ShowSlavya( emotion, outfit )
  MoveSlavya( SCENE_CENTER_X, SCENE_CENTER_Y )
  ScaleSlavya( 0.4 )
endfunction

function ShowSlavyaAtCenterZoomed(emotion, outfit)
  ShowSlavya( emotion, outfit )
  MoveSlavya( SCENE_CENTER_X, SCENE_CENTER_Y - 75 )
  ScaleSlavya( 0.1 )
endfunction

function ZoomInSlavyaAnimated()
  global slavyaScaleTween as integer
  slavyaScaleTween = CreateTweenCustom(0.5)
  SetTweenCustomFloat1(slavyaScaleTween, 0.1, 0.4, TweenEaseIn1())
  PlayTweenCustom(slavyaScaleTween, 1)
endfunction

function ScaleSlavya( scale as float )
  CharacterScaleByOffset( slavyaChar, scale )
endfunction

function MoveSlavya(x, y)
  CharacterMove( slavyaChar, x, y )
endfunction

function MoveSlavyaToCurrentPos()
  MoveSlavya( currentPos.x, currentPos.y )
endfunction

function InitSlavyaBodyOutfits()
  slavyaChar.outfitForBody[1, OUTFIT_DRESS] = SLAVYA_OUTFIT_B1_DRESS
  slavyaChar.outfitForBody[1, OUTFIT_PIONEER] = SLAVYA_OUTFIT_B1_PIONEER
  slavyaChar.outfitForBody[1, OUTFIT_SWIM] = SLAVYA_OUTFIT_B1_SWIM
  slavyaChar.outfitForBody[1, OUTFIT_SPORT] = SLAVYA_OUTFIT_B1_SPORT

  slavyaChar.outfitForBody[2, OUTFIT_DRESS] = SLAVYA_OUTFIT_B2_DRESS
  slavyaChar.outfitForBody[2, OUTFIT_PIONEER] = SLAVYA_OUTFIT_B2_PIONEER
  slavyaChar.outfitForBody[2, OUTFIT_SWIM] = SLAVYA_OUTFIT_B2_SWIM
  slavyaChar.outfitForBody[2, OUTFIT_SPORT] = SLAVYA_OUTFIT_B2_SPORT

  slavyaChar.outfitForBody[3, OUTFIT_DRESS] = SLAVYA_OUTFIT_B3_DRESS
  slavyaChar.outfitForBody[3, OUTFIT_PIONEER] = SLAVYA_OUTFIT_B3_PIONEER
  slavyaChar.outfitForBody[3, OUTFIT_SWIM] = SLAVYA_OUTFIT_B3_SWIM
  slavyaChar.outfitForBody[3, OUTFIT_SPORT] = SLAVYA_OUTFIT_B3_SPORT

  slavyaChar.outfitForBody[4, OUTFIT_DRESS] = SLAVYA_OUTFIT_B4_DRESS
  slavyaChar.outfitForBody[4, OUTFIT_PIONEER] = SLAVYA_OUTFIT_B4_PIONEER
  slavyaChar.outfitForBody[4, OUTFIT_SWIM] = SLAVYA_OUTFIT_B4_SWIM
  slavyaChar.outfitForBody[4, OUTFIT_SPORT] = SLAVYA_OUTFIT_B4_SPORT
endfunction

function ShowSlavyaAgain()
    ShowSlavya(currentEmotion, currentOutfit)
endfunction  

function HideSlavya()
  CharacterHide( slavyaChar )
endfunction

function ReadSlavyaFromFile( file as integer )
  ReadCharacterFromFile( slavyaChar, file )
  MoveSlavya( slavyaChar.pos.x, slavyaChar.pos.y )
  ScaleSlavya( slavyaChar.scale )
  ShowSlavya( slavyaChar.emotion, slavyaChar.outfit )
endfunction

function WriteSlavyaToFile( file as integer )
  WriteCharacterToFile( slavyaChar, file )
endfunction

function ShowSlavyaNormal()
  CharacterNormal(slavyaChar)  
endfunction

function SelectSlavyaAsActiveCharacter()
  activeCharId = slavyaChar.id  
  activeCharacter = slavyaChar
  slavyaChar.selected = YES
endfunction

function DeselectSlavya()
  slavyaChar.selected = NO
  ShowSlavyaNormal()
endfunction

function IsSlavyaSelected()
  res as integer
  res = slavyaChar.selected
endfunction res

function ShowSlavyaHighlighted()
  CharacterHighlight( slavyaChar )  
endfunction

