#constant ALISA_BODY_SPRITE 10003
#constant ALISA_EMOTION_SPRITE 10004
#constant ALISA_OUTFIT_SPRITE 10005

#constant ALISA_BODY_1_IMG "dv_1_body.png"
#constant ALISA_BODY_2_IMG "dv_2_body.png"
#constant ALISA_BODY_3_IMG "dv_3_body.png"
#constant ALISA_BODY_4_IMG "dv_4_body.png"
#constant ALISA_BODY_5_IMG "dv_5_body.png"

#constant ALISA_EMOTION_CRY_B1_IMG "dv_1_cry.png"
#constant ALISA_EMOTION_SCARED_B1_IMG "dv_1_scared.png"
#constant ALISA_EMOTION_SHOCKED_B1_IMG "dv_1_shocked.png"
#constant ALISA_EMOTION_SURPRISE_B1_IMG "dv_1_surprise.png"
#constant ALISA_EMOTION_GRIN_B2_IMG "dv_2_grin.png"
#constant ALISA_EMOTION_GUILTY_B3_IMG "dv_3_guilty.png"
#constant ALISA_EMOTION_SAD_B3_IMG "dv_3_sad.png"
#constant ALISA_EMOTION_SHY_B3_IMG "dv_3_shy.png"
#constant ALISA_EMOTION_LAUGH_B4_IMG "dv_4_laugh.png"
#constant ALISA_EMOTION_NORMAL_B4_IMG "dv_4_normal.png"
#constant ALISA_EMOTION_SMILE_B4_IMG "dv_4_smile.png"
#constant ALISA_EMOTION_ANGRY_B5_IMG "dv_5_angry.png"
#constant ALISA_EMOTION_RAGE_B5_IMG "dv_5_rage.png"

#constant ALISA_OUTFIT_B1_PIONEER "dv_1_pioneer.png"
#constant ALISA_OUTFIT_B1_PIONEER2 "dv_1_pioneer2.png"
#constant ALISA_OUTFIT_B2_PIONEER "dv_2_pioneer.png"
#constant ALISA_OUTFIT_B2_PIONEER2 "dv_2_pioneer2.png"
#constant ALISA_OUTFIT_B3_PIONEER "dv_3_pioneer.png"
#constant ALISA_OUTFIT_B3_PIONEER2 "dv_3_pioneer2.png"
#constant ALISA_OUTFIT_B4_PIONEER "dv_4_pioneer.png"
#constant ALISA_OUTFIT_B4_PIONEER2 "dv_4_pioneer2.png"
#constant ALISA_OUTFIT_B5_PIONEER "dv_5_pioneer.png"
#constant ALISA_OUTFIT_B5_PIONEER2 "dv_5_pioneer2.png"

#constant ALISA_OUTFIT_B1_SWIM "dv_1_swim.png"
#constant ALISA_OUTFIT_B2_SWIM "dv_2_swim.png"
#constant ALISA_OUTFIT_B3_SWIM "dv_3_swim.png"
#constant ALISA_OUTFIT_B4_SWIM "dv_4_swim.png"


#constant ALISA_PATH "dv/"


#constant ALISA_OFFSET_X 100
#constant ALISA_OFFSET_Y 50

#constant ALISA_DEPTH 20

#constant ALISA_CHAR_NAME "Алиса" `ru

global alisaChar as Character

function InitAlisa()
  alisaChar.id = ALISA_CHAR_ID
  alisaChar.name = ALISA_CHAR_NAME

  alisaChar.depth = ALISA_DEPTH
  
  alisaChar.bodySprite = ALISA_BODY_SPRITE
  alisaChar.emotionSprite = ALISA_EMOTION_SPRITE
  alisaChar.outfitSprite = ALISA_OUTFIT_SPRITE

  LoadSprite(ALISA_BODY_SPRITE, SPRITE_PATH + ALISA_PATH + BODY_PATH + ALISA_BODY_1_IMG)
  SetSpriteDepth(ALISA_BODY_SPRITE, BODY_DEPTH)
  SetSpriteScale(ALISA_BODY_SPRITE, CHAR_SPRITE_SCALE_X, CHAR_SPRITE_SCALE_Y)
  SetSpriteVisible( ALISA_BODY_SPRITE, INVISIBLE )
  SetSpriteOffset( ALISA_BODY_SPRITE, ALISA_OFFSET_X, ALISA_OFFSET_Y )

  LoadSprite(ALISA_EMOTION_SPRITE, SPRITE_PATH + ALISA_PATH + EMOTIONS_PATH + ALISA_EMOTION_SMILE_B4_IMG )
  SetSpriteDepth(ALISA_EMOTION_SPRITE, EMOTIONS_DEPTH)
  SetSpriteScale(ALISA_EMOTION_SPRITE, CHAR_SPRITE_SCALE_X, CHAR_SPRITE_SCALE_Y )
  SetSpriteVisible( ALISA_EMOTION_SPRITE, INVISIBLE )
  SetSpriteOffset( ALISA_EMOTION_SPRITE, ALISA_OFFSET_X, ALISA_OFFSET_Y )

  LoadSprite(ALISA_OUTFIT_SPRITE, SPRITE_PATH + ALISA_PATH + OUTFIT_PATH + ALISA_OUTFIT_B1_PIONEER)
  SetSpriteDepth(ALISA_OUTFIT_SPRITE, OUTFIT_DEPTH)
  SetSpriteScale(ALISA_OUTFIT_SPRITE, CHAR_SPRITE_SCALE_X, CHAR_SPRITE_SCALE_Y )
  SetSpriteVisible( ALISA_OUTFIT_SPRITE, INVISIBLE )
  SetSpriteOffset( ALISA_OUTFIT_SPRITE, ALISA_OFFSET_X, ALISA_OFFSET_Y )
endfunction

function ShowAlisa( emotion, dress )
  PrepareToShowAlisaWith( emotion, dress )
  CharacterShow( alisaChar )
endfunction

function PrepareToShowAlisaWith( emotion, dress )
  body as String
  outfit as String
  emotionFile as string

  alisaChar.emotion = emotion
  alisaChar.outfit = dress  
  
  body = ALISA_BODY_4_IMG
  outfit = ALISA_OUTFIT_B4_PIONEER
  emotionFile = ALISA_EMOTION_NORMAL_B4_IMG

  select emotion
    case EMOTION_ANGRY
      body = ALISA_BODY_5_IMG
      outfit = alisaChar.outfitForBody[5, dress]
      emotionFile = ALISA_EMOTION_ANGRY_B5_IMG
    endcase
    case EMOTION_RAGE
      body = ALISA_BODY_5_IMG
      outfit = alisaChar.outfitForBody[5, dress]
      emotionFile = ALISA_EMOTION_RAGE_B5_IMG
    endcase
    case EMOTION_SMILE
      body = ALISA_BODY_4_IMG
      outfit = alisaChar.outfitForBody[4, dress]
      emotionFile = ALISA_EMOTION_SMILE_B4_IMG
    endcase
    case EMOTION_LAUGH
      body = ALISA_BODY_4_IMG
      outfit = alisaChar.outfitForBody[4, dress]
      emotionFile = ALISA_EMOTION_LAUGH_B4_IMG
    endcase
    case EMOTION_NORMAL
      body = ALISA_BODY_4_IMG
      outfit = alisaChar.outfitForBody[4, dress]
      emotionFile = ALISA_EMOTION_NORMAL_B4_IMG
    endcase
    case EMOTION_SAD
      body = ALISA_BODY_3_IMG
      outfit = alisaChar.outfitForBody[3, dress]
      emotionFile = ALISA_EMOTION_SAD_B3_IMG
    endcase
    case EMOTION_SCARED
      body = ALISA_BODY_1_IMG
      outfit = alisaChar.outfitForBody[1, dress]
      emotionFile = ALISA_EMOTION_SCARED_B1_IMG
    endcase
    case EMOTION_SHOCKED
      body = ALISA_BODY_1_IMG
      outfit = alisaChar.outfitForBody[1, dress]
      emotionFile = ALISA_EMOTION_SHOCKED_B1_IMG
    endcase
    case EMOTION_SHY
      body = ALISA_BODY_3_IMG
      outfit = alisaChar.outfitForBody[3, dress]
      emotionFile = ALISA_EMOTION_SHY_B3_IMG
    endcase
    case EMOTION_SUPRISE
      body = ALISA_BODY_1_IMG
      outfit = alisaChar.outfitForBody[1, dress]
      emotionFile = ALISA_EMOTION_SURPRISE_B1_IMG
    endcase
    case EMOTION_GRIN
      body = ALISA_BODY_2_IMG
      outfit = alisaChar.outfitForBody[2, dress]
      emotionFile = ALISA_EMOTION_GRIN_B2_IMG
    endcase
  endselect  

  alisaChar.bodyFile = SPRITE_PATH + ALISA_PATH + BODY_PATH + body
  alisaChar.outfitFile = SPRITE_PATH + ALISA_PATH + OUTFIT_PATH + outfit
  alisaChar.emotionFile = SPRITE_PATH + ALISA_PATH + EMOTIONS_PATH + emotionFile
endfunction

`## ALISA MANAGEMENT ##
function ShowAlisaAtCenter(emotion, outfit)
  ShowALISA( emotion, outfit )
  MoveALISA( SCENE_CENTER_X, SCENE_CENTER_Y )
  ScaleALISA( 0.4 )
endfunction

function ShowAlisaAtCenterZoomed(emotion, outfit)
  ShowALISA( emotion, outfit )
  MoveALISA( SCENE_CENTER_X, SCENE_CENTER_Y - 75 )
  ScaleALISA( 0.1 )
endfunction

function ZoomInAlisaAnimated()
  global alisaScaleTween as integer
  alisaScaleTween = CreateTweenCustom(0.5)
  SetTweenCustomFloat1(alisaScaleTween, 0.1, 0.4, TweenEaseIn1())
  PlayTweenCustom(alisaScaleTween, 1)
endfunction

function ScaleAlisa( scale as float )
  CharacterScaleByOffset( alisaChar, scale )
endfunction

function MoveAlisa(x, y)
  CharacterMove( alisaChar, x, y )
endfunction

function MoveAlisaToCurrentPos()
  MoveAlisa( currentPos.x, currentPos.y )
endfunction

function InitAlisaBodyOutfits()
  alisaChar.outfitForBody[1, OUTFIT_PIONEER2] = ALISA_OUTFIT_B1_PIONEER2
  alisaChar.outfitForBody[1, OUTFIT_PIONEER] = ALISA_OUTFIT_B1_PIONEER
  alisaChar.outfitForBody[1, OUTFIT_SWIM] = ALISA_OUTFIT_B1_SWIM

  alisaChar.outfitForBody[2, OUTFIT_PIONEER2] = ALISA_OUTFIT_B2_PIONEER2
  alisaChar.outfitForBody[2, OUTFIT_PIONEER] = ALISA_OUTFIT_B2_PIONEER
  alisaChar.outfitForBody[2, OUTFIT_SWIM] = ALISA_OUTFIT_B2_SWIM

  alisaChar.outfitForBody[3, OUTFIT_PIONEER2] = ALISA_OUTFIT_B3_PIONEER2
  alisaChar.outfitForBody[3, OUTFIT_PIONEER] = ALISA_OUTFIT_B3_PIONEER
  alisaChar.outfitForBody[3, OUTFIT_SWIM] = ALISA_OUTFIT_B3_SWIM

  alisaChar.outfitForBody[4, OUTFIT_PIONEER2] = ALISA_OUTFIT_B4_PIONEER2
  alisaChar.outfitForBody[4, OUTFIT_PIONEER] = ALISA_OUTFIT_B4_PIONEER
  alisaChar.outfitForBody[4, OUTFIT_SWIM] = ALISA_OUTFIT_B4_SWIM

  alisaChar.outfitForBody[5, OUTFIT_PIONEER2] = ALISA_OUTFIT_B5_PIONEER2
  alisaChar.outfitForBody[5, OUTFIT_PIONEER] = ALISA_OUTFIT_B5_PIONEER
endfunction

function ShowAlisaAgain()
    ShowAlisa(currentEmotion, currentOutfit)
endfunction  

function ReadAlisaFromFile( file as integer )
  ReadCharacterFromFile( alisaChar, file )
  MoveAlisa( alisaChar.pos.x, alisaChar.pos.y )
  ScaleAlisa( alisaChar.scale )
  ShowAlisa( alisaChar.emotion, alisaChar.outfit )
endfunction

function WriteAlisaToFile( file as integer )
 WriteCharacterToFile( alisaChar, file )
endfunction

function HideAlisa()
  CharacterHide(alisaChar)
endfunction

function ShowAlisaNormal()
  CharacterNormal(alisaChar)  
endfunction

function SelectAlisaAsActiveCharacter()
  activeCharId = alisaChar.id
  activeCharacter = alisaChar
  alisaChar.selected = YES
endfunction

function DeselectAlisa()
  alisaChar.selected = NO
  ShowAlisaNormal()
endfunction

function IsAlisaSelected()
  res as integer
  res = alisaChar.selected
endfunction res

function ShowAlisaHighlighted()
  CharacterHighlight( alisaChar )  
endfunction
