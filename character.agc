`` TYPES ``
Type Character
  id as integer
  emotion as integer
  outfit as integer
  pos as Point
  scale as float
  isVisible as integer  
  selected as integer
  
  name as string

  bodySprite as integer
  emotionSprite as integer
  outfitSprite as integer
  
  bodyFile as String
  emotionFile as String
  outfitFile as String
  
  depth as integer

  outfitForBody as String[MAX_BODIES, MAX_OUTFITS]
EndType



`` FUNCTIONS ``

function CharacterShow( ch ref as Character )
  
  ch.isVisible = VISIBLE

  DeleteSprite(ch.BodySprite)
  ch.BodySprite = LoadSprite(ch.bodyFile)
  DeleteSprite(ch.OutfitSprite)
  ch.OutfitSprite = LoadSprite(ch.outfitFile)
  DeleteSprite(ch.EmotionSprite)
  ch.EmotionSprite = LoadSprite(ch.emotionFile)

  SetSpriteVisible( ch.BodySprite, VISIBLE )
  SetSpriteVisible( ch.EmotionSprite, VISIBLE )
  
  if(nudeMode = NO)
    SetSpriteVisible( ch.OutfitSprite, VISIBLE )
  else
    SetSpriteVisible( ch.OutfitSprite, INVISIBLE )
	endif

  if(fullScreenMode = NO)
    SetSpriteScissor( ch.BodySprite, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - VERTICAL_SHIFT + 15 )
    SetSpriteScissor( ch.EmotionSprite, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - VERTICAL_SHIFT + 15 )
    SetSpriteScissor( ch.OutfitSprite, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - VERTICAL_SHIFT + 15 )
  else
    SetSpriteScissor( ch.BodySprite, 0, 0, 0, 0 )
    SetSpriteScissor( ch.EmotionSprite, 0, 0, 0, 0 )
    SetSpriteScissor( ch.OutfitSprite, 0, 0, 0, 0 )
  endif

  SetSpriteDepth(ch.BodySprite, ch.depth + BODY_DEPTH)
  SetSpriteScale(ch.BodySprite, currentZoom, currentZoom)
  SetSpriteOffset(ch.BodySprite, CHAR_OFFSET_X, CHAR_OFFSET_Y )

  SetSpriteDepth(ch.EmotionSprite, ch.depth + EMOTIONS_DEPTH)
  SetSpriteScale(ch.EmotionSprite, currentZoom, currentZoom )
  SetSpriteOffset(ch.EmotionSprite, CHAR_OFFSET_X, CHAR_OFFSET_Y )

  SetSpriteDepth(ch.OutfitSprite, ch.depth + OUTFIT_DEPTH)
  SetSpriteScale(ch.OutfitSprite, currentZoom, currentZoom )
  SetSpriteOffset(ch.OutfitSprite, CHAR_OFFSET_X, CHAR_OFFSET_Y )
  
  CharacterMove(ch, currentPos.x, currentPos.y)
endfunction

function CharacterMove( ch ref as Character, x, y )
  ch.pos.x = x
  ch.pos.y = y
  
  SetSpritePositionByOffset( ch.OutfitSprite, x, y )
  SetSpritePositionByOffset( ch.EmotionSprite, x, y )
  SetSpritePositionByOffset( ch.BodySprite, x, y )
endfunction

function CharacterScaleByOffset( ch ref as Character, scale as float)
  ch.scale = scale 
  
  SetSpriteScaleByOffset( ch.OutfitSprite, scale, scale )
  SetSpriteScaleByOffset( ch.EmotionSprite, scale, scale )
  SetSpriteScaleByOffset( ch.BodySprite, scale, scale )
endfunction

function CharacterScale( ch ref as Character, scale as float )
  ch.scale = scale 
  
  SetSpriteScale( ch.OutfitSprite, scale, scale )
  SetSpriteScale( ch.EmotionSprite, scale, scale )
  SetSpriteScale( ch.BodySprite, scale, scale )
endfunction

function CharacterHighlight( ch ref as Character )
  SetSpriteColorBlue( ch.BodySprite, HIGHLIGHT_BLUE )
  SetSpriteColorBlue( ch.EmotionSprite, HIGHLIGHT_BLUE )
  SetSpriteColorBlue( ch.OutfitSprite, HIGHLIGHT_BLUE )
endfunction

function CharacterNormal( ch ref as Character )
  SetSpriteColorBlue( ch.BodySprite, NORMAL_BLUE )
  SetSpriteColorBlue( ch.EmotionSprite, NORMAL_BLUE )
  SetSpriteColorBlue( ch.OutfitSprite, NORMAL_BLUE )
endfunction

function ReadCharacterFromFile( ch ref as Character, file as integer )
  ch.id = ReadInteger(file)
  ch.emotion = ReadInteger(file)
  ch.outfit = ReadInteger(file)
  ch.depth = ReadInteger(file)
  ch.pos = ReadPositionFromFile(file)
  ch.scale = ReadFloat(file)
endfunction

function WriteCharacterToFile( ch ref as Character, file as integer )
  WriteInteger( file, ch.id )
  WriteInteger( file, ch.emotion )
  WriteInteger( file, ch.outfit )
  WriteInteger( file, ch.depth )
  WritePositionToFile( ch.pos, file )
  WriteFloat( file, ch.scale )
endfunction

function CharacterHide( ch ref as Character )
  
  ch.isVisible = INVISIBLE 
  
  SetSpriteVisible( ch.bodySprite, INVISIBLE )
  SetSpriteVisible( ch.emotionSprite, INVISIBLE )
  SetSpriteVisible( ch.outfitSprite, INVISIBLE )
endfunction


`` ACTIVE CHARACTER ``
global activeCharacter as Character

function GetActiveCharacter()
  res as Character
  res = activeCharacter
endfunction res

function ScaleActiveCharacter( scale as float )
  CharacterScaleByOffset( activeCharacter, scale )
  UpdateActiveCharacter()
endfunction

function MoveActiveCharacterToCurrentPos(x as float, y as float)
  CharacterMove( activeCharacter, x, y )
  UpdateActiveCharacter()
endfunction

function GetActiveCharacterScale()
  res as float 
  res = activeCharacter.scale
endfunction res

function GetActiveCharacterPos()
  res as Point
  res = activeCharacter.pos
endfunction res

function GetActiveCharacterEmotion()
  res as integer
  res = activeCharacter.emotion
endfunction res

function GetActiveCharacterOutfit()
  res as integer
  res = activeCharacter.outfit
endfunction res

function SetActiveCharacterOutfit( outfit as integer )
  activeCharacter.outfit = outfit
  UpdateActiveCharacter()
endfunction

function SetActiveCharacterEmotion( emotion as integer )
  activeCharacter.emotion = emotion
  UpdateActiveCharacter()
endfunction
